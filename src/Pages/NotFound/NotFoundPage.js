import React from 'react'


export default function NotFoundPage() {
  return (
    <div className='flex justify-center items-center h-screen w-screen'>
        <h1 className=' text-red-600 text-4xl font-medium animate-bounce'>404</h1>
    </div>
  )
}
