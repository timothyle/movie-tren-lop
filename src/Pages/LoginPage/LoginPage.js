
import React from 'react';
import { Button, Form, Input, message } from 'antd';
import { userService } from '../../services/userServices';
import { useNavigate ,} from 'react-router-dom';
import { useDispatch } from 'react-redux'
import { userLocalService } from '../../services/localStorageService';
import Lottie from "lottie-react";
import bg_animation from '../../assets/90315-christmas-tree.json'
import { setLoginAction, setLoginActionService } from '../../redux/actions/userAction';

export default function LoginPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const onFinish = (values) => {
        console.log('Success:', values);
        userService
        .postDangNhap(values)
        .then((res) => {
            dispatch(setLoginAction(res.data.content))
            console.log(res)
            message.success("Đăng nhập thành công");
            userLocalService.set(res.data.content)
            // window.location.heft => reload toan bo trang
            // window.location.href = "/"
            // dung Navigate de khong bi load trang
            setTimeout(() => {
            navigate("/");
        },1000)
    })
        .catch((err) => {
            console.log(err)
            message.error("Đã có lỗi xảy ra")
        })
      };
    const onFinishReduxThunk = (value) => {
        let onNavigate = () => {
            setTimeout(() => {
                navigate("/");
            },1000)
        }
        dispatch(setLoginActionService(value, onNavigate ))
    };


    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };
  return (
    <div className='w-screen h-screen flex justify-center items-center'>
        <div className='container p-5 flex'>
            <div className='h-full w-1/2'>
                <Lottie animationData={bg_animation} loop={true} />
            </div>
            <div className='h-full w-1/2'>
            <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 24,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout='vertical'
            >
            <Form.Item
                label="Username"
                name="taiKhoan"
                rules={[
                {
                    required: true,
                    message: 'Please input your username!',
                },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="matKhau"
                rules={[
                {
                    required: true,
                    message: 'Please input your password!',
                },
                ]}
            >
                <Input.Password />
            </Form.Item>
            <Form.Item
            className='text-center'
                wrapperCol={{
                span: 24,
                }}
            >
                <Button type="primary" className='text-white' danger htmlType="submit">
                Submit
                </Button>
            </Form.Item>
            </Form>
            </div>
        </div>
    </div>
  )
}
