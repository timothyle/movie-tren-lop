import React from 'react'

import { Card } from 'antd';
import { NavLink } from 'react-router-dom';

const { Meta } = Card;

export default function MovieList( { movieArr }) {
    const renderMovieList = () => {
    return movieArr.slice(0, 15).map((item) => {
        let {hinhAnh,tenPhim,moTa,maPhim} = item;
        return (
            <div>
        <Card
            hoverable
            style={{ width: "100%"}}
            cover={<img 
                alt="example" 
            src={hinhAnh} className="h-80 object-cover"/>}
        >
          {/* <Meta title={tenPhim} className="h-100" description={moTa.length < 60 ? moTa :             moTa.slice(0,60)+ "..."} /> */}
                            <NavLink to={`/detail/${maPhim}`} className={"bg-red-500 px-5 py-2 rounded text-white mx-auto p shadow hover:bg-blue-500 transition hover:text-white"}>
                Xem Chi Tiết
            </NavLink>
        </Card>
    </div>
  )
})
    }
return <div className='grid grid-cols-5 gap-5'>
    {renderMovieList()}
    </div> 
}
