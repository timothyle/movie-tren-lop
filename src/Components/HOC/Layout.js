import React, { Children } from 'react'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'

export default function Layout({children}) {
  return (
    <div className='space-y-5'>
        <Footer/>
        <Header/>
        {children}
    </div>
  )
}
