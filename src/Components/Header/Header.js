import React, { Component } from 'react'
import UserNav from './UserNav'
import { NavLink } from 'react-router-dom'

export default class Header extends Component {
  render() {
    return (
      <div className='flex px-10 py-5 shadow shadow-blue-500 items-center justify-between'>
            <NavLink to="/">
            <span className='text-red-600 font-medium text-xl'>CyberFlix</span> 
            </NavLink>
            
            <UserNav/>       
      </div>
    )
  }
}
