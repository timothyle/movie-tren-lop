import React, { Component } from 'react'
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import {userLocalService} from "../../services/localStorageService"

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor
  });

  let handleLogout = () => {
    userLocalService.remove();
    window.location.href = "/login";
  };
  const renderContent = () => {
    if(user){
      return(
        <>
          <span>{user.hoTen}</span>
          <button className='border-2 rounded border-black px-5 py-2' onClick={handleLogout}>Đăng Xuất</button>
        </>
      )
    } else {
      return (
        <>
        <NavLink to="/login"><button className='border-2 rounded  border-black  px-5 py-2'>Đăng     Nhập{" "}</button></NavLink>
        <button className='border-2 rounded  border-black  px-5 py-2'>Đăng Ký</button>
        </>
      )
    }
  }

    return <div className='space-x-5'>{renderContent()}</div>
}
