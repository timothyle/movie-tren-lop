import "./App.css";
import {BrowserRouter, Routes,Route} from 'react-router-dom';
import LoginPage from "./Pages/LoginPage/LoginPage";
import HomePage from "./Pages/HomePage/HomePage";
import NotFoundPage from "./Pages/NotFound/NotFoundPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Layout from "./Components/HOC/Layout";

function App() {
  return (
    <div>
     <BrowserRouter>
        <Routes>
            <Route path="/" element={<Layout><HomePage/></Layout>}/>
            <Route path="/login" element={<LoginPage/>}/>
            <Route path="/detail/:id" element={<Layout><DetailPage/></Layout>}/>
            <Route path="*" element={<NotFoundPage/>}/>
        </Routes>
     </BrowserRouter>
    </div>
  );
}

export default App;
// redux toolkit
